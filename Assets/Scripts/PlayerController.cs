﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public ParticleSystem explosion;
    public ParticleSystem dirtTracks;
    public AudioClip crash;
    public AudioClip jump;
    private AudioSource playerAudio;
    private Rigidbody playerRb;
    public float jumpForce;
    public float gravityModifier;
    public bool isOnGround = true;
    public bool gameOver = false;
    private Animator playerAnim;

    // Start is called before the first frame update
    void Start()
    {
        playerAudio = GetComponent<AudioSource>();
        playerRb = GetComponent<Rigidbody>();
        Physics.gravity *= gravityModifier;
        playerAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround && !gameOver)
        {
            playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isOnGround = false;
            playerAnim.SetTrigger("Jump_trig");
            dirtTracks.Stop();
            playerAudio.PlayOneShot(jump, 1.0f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
            if (!gameOver)
            {
                dirtTracks.Play();
            }
        }
        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            dirtTracks.Stop();
            Debug.Log("You have lost.");
            gameOver = true;
            playerAnim.SetBool("Death_b", true);
            playerAnim.SetInteger("DeathType_int", 1);
            explosion.Play();
            playerAudio.PlayOneShot(crash, 1.0f);
        }
    }
}
